# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.1.1 (2020-11-06)


### Features

* allow control of INPUT and FORWARD policies ([d0c796e](https://gitlab.com/guardianproject-ops/ansible-firewall-docker/commit/d0c796ee155ae4b014b61597bb83f58cde948d12))

## [0.1.0] - 2020-05-11

### Added

- This CHANGELOG

### Changed

n/a

### Removed

n/a

[Unreleased]: https://gitlab.com/guardianproject-ops/ansible-firewall-docker/compare/0.1.0...master
[0.1.0]: https://gitlab.com/guardianproject-ops/ansible-firewall-docker/-/tags/0.1.0
