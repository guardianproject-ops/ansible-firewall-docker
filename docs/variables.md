## Role Variables

* `firewall_open_ports_tcp`: `[]` - list of tcp ports to open



* `firewall_open_ports_udp`: `[]` - list of udp ports to open



* `firewall_wan_interface`: `"{{ ansible_default_ipv4.interface }}"` - 



* `firewall_logging_enabled`: `false` - 



* `firewall_custom_filter`: `[]` - is a list of maps you can use to build custom allow rules. The available keys are as follows and are straight from the iptables manpage: `proto`, `port`, `iface`, `source`, `dest`, `label` -  added as a comment, REQUIRED



* `firewall_input_policy`: `ACCEPT` - one of [DROP, ACCEPT]



* `firewall_forward_policy`: `DROP` - one of [DROP, ACCEPT]


